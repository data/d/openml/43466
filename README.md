# OpenML dataset: Bank-Note-Authentication-UCI

https://www.openml.org/d/43466

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Data were extracted from images that were taken from genuine and forged banknote-like specimens. For digitization, an industrial camera usually used for print inspection was used. The final images have 400x 400 pixels. Due to the object lens and distance to the investigated object gray-scale pictures with a resolution of about 660 dpi were gained. Wavelet Transform tool were used to extract features from images.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43466) of an [OpenML dataset](https://www.openml.org/d/43466). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43466/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43466/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43466/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

